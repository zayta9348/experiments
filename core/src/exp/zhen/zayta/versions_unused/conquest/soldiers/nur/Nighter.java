package exp.zhen.zayta.versions_unused.conquest.soldiers.nur;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import exp.zhen.zayta.versions_unused.conquest.soldiers.Soldier;

public class Nighter extends Soldier {


    public Nighter(String name, TextureRegion textureRegion, int hp, int atk, int def) {
        super(name, textureRegion, hp, atk, def);
    }

    @Override
    public void activateAbility(Soldier target) {

    }

}
