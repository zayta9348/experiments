package exp.zhen.zayta.versions_unused.conquest.soldiers.utsubyo;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

import exp.zhen.zayta.versions_unused.conquest.soldiers.Soldier;

public class Monster extends Soldier {
    public Monster(TextureRegion textureRegion, int hp, int atk, int def) {
        super(textureRegion, hp, atk, def);
    }

    @Override
    public void activateAbility(Soldier target) {

    }

}
