package exp.zhen.zayta.main.assets;

public class RegionNames {

    public static final String PANEL = "window";

    //maps
    public static final String MAPS_MEMLAB1 = "game/experiment/maps/memLab/memLab1.tmx";
    public static final String MAPS_MEMLAB2 = "game/experiment/maps/memLab/memLab2.tmx";
    public static final String MAPS_MEMLABBIG = "game/experiment/maps/memLab/memLabBig.tmx";

    //backgrounds

    //misc
    public static final String BEACH ="scene_backgrounds/Ardentryst-Intro_Kiripan_Beach_Fade";
    public static final String DARK_PATH = "scene_backgrounds/darkpath";
    public static final String FULL_SCANNER = "scene_backgrounds/fullscanner";
    public static final String RUINED_CITY = "scene_backgrounds/ruined_city";
    public static final String SQUARE_FLOOR = "scene_backgrounds/square_pattern";
    public static final String SKY_DAY = "scene_backgrounds/sky/day sky";
    public static final String SKY_NIGHT = "scene_backgrounds/sky/night";
    public static final String SKY_SUNRISE = "scene_backgrounds/sky/sunrise_parallax";

    //characters
    public static final String LORALE = "characters/lorale";
    public static final String LETRA ="characters/letra";

    public static final String TENYU ="characters/tenyu";

    public static final String TARIA ="characters/taria";


    public static final String CIVILIAN = "characters/anoni";
    public static final String CIVILIAN_RED = "characters/red_anoni";
    public static final String CIVILIAN_PURPLE = "characters/purple_anoni";



    public static final String [] MONSTERS = {"monsters/zemus"};

    //blobs
    public static final String FIRE_BLOB_FRONT = "flames/firecreature/blob_front";
    public static final String FIRE_BLOB_PREFLARE = "flames/firecreature/blob_flare_ready";
    public static final String FIRE_BLOB_FLARE = "flames/firecreature/blob_flare";
    public static final String FIRE_BLOB_BACK = "flames/firecreature/blob_back";
    public static final String FIRE_BLOB_LEFT = "flames/firecreature/blob_move_left";
    public static final String FIRE_BLOB_RIGHT = "flames/firecreature/blob_move_right";


    //magic
    public static final String EXPLOSION_FIRE = "flames/explosion_fire";
    public static final String PRESET_EXPLOSION_FIRE = "flames/preset_explosion_fire";
    public static final String RING_FIRE = "flames/ring_fire";
    public static final String PRESET_RING_FIRE = "flames/preset_ring_fire";


    public static final String [] LIGHT_EFFECTS = {"light_effects/light_effects","light_effects/light_effects2","light_effects/light_effects3","light_effects/light_effects4","light_effects/light_effects5"};



    //emotes
    //yellow
    public static final String EMOTES_ANGRY = "emotes/cubikopp/icon_angry";
    public static final String EMOTES_BIGSMILE = "emotes/cubikopp/icon_bigsmile";
    public static final String EMOTES_BLUSH = "emotes/cubikopp/icon_blush";
    public static final String EMOTES_CONFUSED = "emotes/cubikopp/icon_confused";
    public static final String EMOTES_COOL = "emotes/cubikopp/icon_cool";
    public static final String EMOTES_CRY = "emotes/cubikopp/icon_cry";
    public static final String EMOTES_EEK = "emotes/cubikopp/icon_eek";
    public static final String EMOTES_IMPORTANT = "emotes/cubikopp/icon_important";
    public static final String EMOTES_KISS = "emotes/cubikopp/icon_kiss";
    public static final String EMOTES_LOL = "emotes/cubikopp/icon_lol";
    public static final String EMOTES_NEUTRAL = "emotes/cubikopp/icon_neutral";
    public static final String EMOTES_SAD = "emotes/cubikopp/icon_sad";
    public static final String EMOTES_SICK = "emotes/cubikopp/icon_sick";
    public static final String EMOTES_SMILE = "emotes/cubikopp/icon_smile";
    public static final String EMOTES_SURPRISED = "emotes/cubikopp/icon_surprised";
    public static final String EMOTES_THINK = "emotes/cubikopp/icon_think";
    public static final String EMOTES_TONGUE = "emotes/cubikopp/icon_tongue";
    public static final String EMOTES_TWISTED = "emotes/cubikopp/icon_twisted";
    public static final String EMOTES_WINK = "emotes/cubikopp/icon_wink";



    public static final String [] BLOCKS = {};
    public static final String STONE = "stones_blackpplstone";


}
