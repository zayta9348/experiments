package exp.zhen.zayta.main.game.essence_lab.engine.render.animation.sprite;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import exp.zhen.zayta.main.game.essence_lab.engine.movement.Direction;
import exp.zhen.zayta.main.game.essence_lab.common.Mappers;
import exp.zhen.zayta.main.game.essence_lab.engine.movement.component.VelocityComponent;
import exp.zhen.zayta.main.game.essence_lab.engine.render.animation.TextureComponent;

public class SpriteAnimationSystem extends IteratingSystem {

    private static Family FAMILY= Family.all(
            SpriteAnimationComponent.class,
            TextureComponent.class,
            VelocityComponent.class
    ).get();
    public SpriteAnimationSystem() {
        super(FAMILY);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        VelocityComponent movement = Mappers.MOVEMENT.get(entity);//todo figure out how to animate without polling for direction each time.
        Direction direction = movement.getDirection();
        SpriteAnimationComponent animation = Mappers.SPRITE_ANIMATION.get(entity);
        if(direction!=Direction.none) {
            animation.updateCurrentTime(deltaTime);
        }
        animation.setFrames(movement.getDirection());

        TextureRegion frame = animation.getFrame();
        TextureComponent texture = Mappers.TEXTURE.get(entity);
        texture.setRegion(frame);


    }
}
