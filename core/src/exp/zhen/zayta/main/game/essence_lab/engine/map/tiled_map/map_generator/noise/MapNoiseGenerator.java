package exp.zhen.zayta.main.game.essence_lab.engine.map.tiled_map.map_generator.noise;

public interface MapNoiseGenerator {
	float[][] getMap();
}
