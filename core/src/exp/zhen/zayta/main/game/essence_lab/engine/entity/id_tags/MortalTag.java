package exp.zhen.zayta.main.game.essence_lab.engine.entity.id_tags;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Pool;

public class MortalTag implements Component,Pool.Poolable {

    private boolean hit;

    @Override
    public void reset() {
        hit = false;
    }

    public boolean isHit() {
        return hit;
    }

    public void setHit(boolean hit) {
        this.hit = hit;
    }

}
