package exp.zhen.zayta.main.game.essence_lab.engine.entity.components.properties.explosion;

import com.badlogic.ashley.core.Component;

public class ExplosiveComponent implements Component {

    private int power=10;

    public void setPower(int power) {
        this.power = power;
    }

    public int getPower() {
        return power;
    }
}
