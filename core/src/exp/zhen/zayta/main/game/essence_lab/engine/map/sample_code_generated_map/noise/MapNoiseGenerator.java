package exp.zhen.zayta.main.game.essence_lab.engine.map.sample_code_generated_map.noise;

public interface MapNoiseGenerator {
	float[][] getMap();
}
